package modelo;


public class Bomba {
   private Gasolina gas;
   private int numBomba;
   private float capacidadBomba;
   private float acumuladorLitros;
   
   public Bomba(){
        this.numBomba=0;
        this.gas= null;
        this.capacidadBomba=0;
        this.acumuladorLitros=0;
   }

    public Bomba(Gasolina gas, int numBomba, float capacidadBomba, float acumuladorLitros) {
        this.gas = gas;
        this.numBomba = numBomba;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorLitros = acumuladorLitros;
    }
    public Bomba(Bomba aux){
        this.gas=aux.gas;
        this.numBomba=aux.numBomba;
        this.capacidadBomba=aux.capacidadBomba;
        this.acumuladorLitros=aux.acumuladorLitros;
    }

    public Gasolina getGas() {
        return gas;
    }

    public void setGas(Gasolina gas) {
        this.gas = gas;
    }

    public int getNumBomba() {
        return numBomba;
    }

    public void setNumBomba(int numBomba) {
        this.numBomba = numBomba;
    }

    public float getCapacidadBomba() {
        return capacidadBomba;
    }

    public void setCapacidadBomba(float capacidadBomba) {
        this.capacidadBomba = capacidadBomba;
    }

    public float getAcumuladorLitros() {
        return acumuladorLitros;
    }

    public void setAcumuladorLitros(float acumuladorLitros) {
        this.acumuladorLitros = acumuladorLitros;
    }
    
    public void iniciarBomba( int numBomba,Gasolina gas, float capacidadBomba, float acumuladorLitros) {
        this.numBomba = numBomba;
        this.gas = gas;
        this.capacidadBomba = capacidadBomba;
        this.acumuladorLitros = acumuladorLitros;
    }
    
    public float inventarioGasolina(){
        return capacidadBomba - acumuladorLitros;
    }
    
    public float venderGasolina(float gasolinaC){
        if(inventarioGasolina()<gasolinaC){
            return 0;
        }
        else{
            this.acumuladorLitros+=gasolinaC;
            return gasolinaC * gas.getPrecio();
        }
    }
    
    public float ventasTotales(){
        return acumuladorLitros * gas.getPrecio();
    }
   
}
