package modelo;

public class Gasolina {
    private int idGasolina;
    private String tipo;
    private float precio;
    
    public Gasolina(){
        this.idGasolina=0;
        this.tipo="";
        this.precio=0;
    }
    public Gasolina(int idGasolina, String tipo, float precio){
        this.idGasolina=idGasolina;
        this.tipo=tipo;
        this.precio=precio;
    }
    public Gasolina(Gasolina aux){
        this.idGasolina=aux.idGasolina;
        this.tipo=aux.tipo;
        this.precio=aux.precio;
    }
    
    public void setIdGasolina(int Idgas){
        this.idGasolina=Idgas;
    }
    public int getIdGasolina(){
        return idGasolina; 
    }
    
    public void setTipo(String type){
        this.tipo=type;
    }
    public String getTipo(){
        return tipo;
    }
    
    public void setPrecio(float price){
        this.precio=price;
    }
    public float getPrecio(){
        return precio;
    }
}   
