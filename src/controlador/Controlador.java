
package controlador;

import modelo.Bomba;
import modelo.Gasolina;
import vista.dlgVenta;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    
    private Bomba bomb;
    private dlgVenta vista;
    private int c;
    
    public Controlador(Bomba bomb, dlgVenta vista){
        this.bomb = bomb;
        this.vista=vista;
        
        this.vista.btnIniciarBomba.addActionListener(this);
        this.vista.btnRegistrar.addActionListener(this);
    }
    
    public void iniciarVista(){
        this.vista.setTitle("BOMBA");
        this.vista.setSize(715, 515);
        this.vista.setVisible(true);
    }
    
    private boolean isVacio(){
        if(this.vista.txtNumBomba.getText().equals("") || this.vista.txtPrecioVenta.getText().equals("")){
            return true;
        }
        else{
            return false;
        }
    }
    
    private int isValidoIniciarBomba(){
        try{
            if(Float.parseFloat(this.vista.txtNumBomba.getText()) <= 0 || 
               Float.parseFloat(this.vista.txtPrecioVenta.getText()) <= 0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
            return 2;
        }
        return 0;
    }

    private int isValidoRegistrar(){
        try{
            if(Float.parseFloat(this.vista.txtCantidad.getText()) <= 0){
                return 1;
            }
        }
        catch(NumberFormatException ex){
            return 2;
        }
        return 0;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==this.vista.btnIniciarBomba){
           if(this.isVacio()==true){
               JOptionPane.showMessageDialog(vista, "Imposible iniciar,Campos vacios");
           }
           else{
               if(this.isValidoIniciarBomba()==1){
                   JOptionPane.showMessageDialog(vista, "ERROR DEBIDO A NUMEROS NEGATIVOS O IGUALES A 0");
                   return;
               }
               else if(this.isValidoIniciarBomba()==2){
                   JOptionPane.showMessageDialog(vista, "ERROR DEBIDO A LETRAS EN CAMPOS NUMERICOS");
                   return;
               }
               this.bomb.iniciarBomba(Integer.parseInt(this.vista.txtNumBomba.getText()), 
                    new Gasolina(this.vista.cbxTipo.getSelectedIndex() +1,
                    this.vista.cbxTipo.getSelectedItem().toString(),
                    Float.parseFloat(this.vista.txtPrecioVenta.getText())),
                    this.vista.jsCantidad.getValue(), 0);
                this.vista.btnIniciarBomba.setEnabled(false);
                this.vista.txtNumBomba.setEnabled(false);
                this.vista.cbxTipo.setEnabled(false);
                this.vista.txtContadorVentas.setEnabled(false);
                this.vista.jsCantidad.setEnabled(false);
                this.vista.txtPrecioVenta.setEnabled(false);

                this.vista.txtCantidad.setEnabled(true);
                this.vista.btnRegistrar.setEnabled(true);
           }
            
            
        }
        
        if(e.getSource()==this.vista.btnRegistrar){
            if(this.vista.txtCantidad.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Imposible iniciar,Campos vacios");
                return;
            }
            if(this.isValidoRegistrar()==1){
                JOptionPane.showMessageDialog(vista, "ERROR DEBIDO A NUMEROS NEGATIVOS O IGUALES A 0");
                return;
            }
            else if(this.isValidoRegistrar()==2){
                JOptionPane.showMessageDialog(vista, "ERROR DEBIDO A LETRAS EN CAMPOS NUMERICOS");
                return;
            }
            float venta = this.bomb.venderGasolina(Float.parseFloat(this.vista.txtCantidad.getText()));
            if(venta != 0.0f){
                this.vista.lblCosto.setText(String.valueOf(venta));
                this.vista.lblTotalV.setText(String.valueOf(this.bomb.ventasTotales()));
                this.c++;
                this.vista.txtContadorVentas.setText(String.valueOf(this.c));
                this.vista.jsCantidad.setValue((int)this.bomb.inventarioGasolina());
            }
            else{
                JOptionPane.showMessageDialog(vista, "Imposible realizar venta, Bomba vacia");
            }
            
            
        }
    }
    
    public static void main(String[] args) {
        dlgVenta vista = new dlgVenta(new JFrame(), true);
        Bomba bomb = new Bomba();
        Controlador contra = new Controlador(bomb, vista);
        contra.iniciarVista();
    }
}

//PROYECTO FINALIZADO QUEZADA RAMOS JULIO EMILIANO 2019030880